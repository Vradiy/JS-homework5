let mainObj = {
    name:"Den",
    age:19,
    adress:{
        street:{
            name:"streetName",
            houseNumber:12
        }
    }
}

function cloneObj (obj){
    let resultObj = {}
     if(typeof obj !== 'object'|| obj === null){
        return obj;
     }

     for(let key in obj){
        resultObj[key] = cloneObj(obj[key])       
     }
     return resultObj;
}

let objNew = cloneObj(mainObj);
console.log(objNew);



// let demo = {}
// demo["newFiled"] = 123
// demo = {newFiled:123}